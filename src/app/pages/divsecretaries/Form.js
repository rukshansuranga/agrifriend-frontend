import React from "react";
import axios from "axios";

import { Formik, Form, Field, ErrorMessage } from "formik";

const DivForm = () => {
  return (
    <>
      <h1>Divisional Secratary</h1>

      <Formik
        initialValues={{
          name: "",
          email: "",
          phone: "",
          contactPerson: "",
          contactMobile: "",
          address: "",
        }}
        onSubmit={(values, { setSubmitting }) => {
          axios
            .post(`http://localhost:61685/api/DivSecretaries`, values)
            .then((res) => {
              console.log(res);
              console.log(res.data);
            });
        }}
      >
        <Form>
          <Field name="name" />
          <Field name="email" />
          <Field name="phone" />
          <Field name="contactPerson" />
          <Field name="contactMobile" />
          <Field name="address" />
          <button type="submit">Submit</button>
        </Form>
      </Formik>
    </>
  );
};

export default DivForm;
