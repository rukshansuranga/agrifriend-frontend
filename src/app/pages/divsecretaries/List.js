import React, { useState, useEffect } from "react";
import axios from "axios";

const List = () => {
  const [data, setData] = useState(null);

  useEffect(() => {
    getDivisions();
  });

  const getDivisions = async () => {
    const result = await axios(`http://localhost:61685/api/DivSecretaries`);
    setData(result.data);
  };

  return (
    <>
      <table>
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Phone</th>
          <th>Email</th>
          <th>ContactPerson</th>
          <th>ContactMobile</th>
        </tr>
        {data &&
          data.map((division) => (
            <tr>
              <td>{division.id}</td>
              <td>{division.name}</td>
              <td>{division.phone}</td>
              <td>{division.email}</td>
              <td>{division.contactPerson}</td>
              <td>{division.contactMobile}</td>
            </tr>
          ))}
      </table>
    </>
  );
};

export default List;
