import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import DivForm from "./app/pages/divsecretaries/Form";
import DivList from "./app/pages/divsecretaries/List";
import Head from "./app/pages/Home";
import Footer from "./app/pages/Footer";

function App() {
  return (
    <Router>
      <Head />
      <Switch>
        <Route path="/divisions" component={DivList} />
        <Route path="/divisions/create" component={DivForm} />
      </Switch>
      <Footer />
    </Router>
  );
}

export default App;
